<div class="row">
    <div class="col s12 offset-m1 m10">
        <div class="card">
            <div class="card-content">
                <span class="card-title">Lista dostępnych sal</span>
            </div>
            <div class="card-action">
                <table class="striped centered" style="cursor: default;">
                    <thead>
                        <tr>
                            <th>Lp.</th>
                            <th>Numer sali</th>
                            <th>Liczba miejsc</th>
                            <th>Typ sali</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                        <?php echo \App\Models\Loader::getFullRoomListTable(); ?>
                        
                    </tbody>
                </table>
                <div class="table-legend">
                    <i class="material-icons right" title="sala komputerowa">&#xE30C;</i>sala komputerowa
                </div>
                <div class="table-legend">
                    <i class="material-icons right" title="sala seminaryjna/wykładowa">&#xE7EF;</i>sala seminaryjna/wykładowa
                </div>
            </div>
        </div>
    </div>
</div>