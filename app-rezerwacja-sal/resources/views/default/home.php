<?php App\Models\Action::homeAction(); ?>

<div class="row">
    <div class="col s12 m6">
        <div class="card">
            <div class="card-content">
                <span class="card-title">Rezerwacje wg numeru sali</span>
                <p>Wyświetl aktualne rezerwacje wybranej sali.</p>
            </div>
            <div class="card-action">
                <div class="input-field col s12">
                    <select name="roomID" id="roomID">
                        <option value="" disabled selected>wybierz numer sali</option>
                        
                        <?php echo App\Models\Loader::getRoomsList(); ?>
                        
                    </select>                    
                </div>
                <div>
                    <button class="waves-effect waves-light btn green" type="button" onclick="getRoomUrl('roomID', '<?php getSubDomain(); ?>')">WYŚWIETL</button>
                </div>
            </div>
        </div>
    </div>
    <div class="col s12 m6">
        <div class="card">
            <div class="card-content">
                <span class="card-title">Rezerwacje wg daty</span>
                <p>Wyświetl aktualne rezerwacje w wybranym dniu.</p>
            </div>
            <div class="card-action">
                <div class="input-field">                    
                    <input id="dateTime" name="date" type="text" class="dateTime validate" required />
                    <label for="dateTime">data rezerwacji</label>
                </div>
                <div>
                    <button class="waves-effect waves-light btn green" type="button" onclick="getRoomUrl('dateTime', '<?php getSubDomain(); ?>')">WYŚWIETL</button>
                </div>
            </div>
        </div>
    </div>
</div>

<?php echo App\Models\Loader::getLastReservations(); ?>
