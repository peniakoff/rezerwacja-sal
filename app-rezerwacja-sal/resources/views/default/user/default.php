<?php 

App\Models\Action::userReservationsAction();
App\Models\Action::infoCardAction();

?>

<div class="row">
    <div class="col s12 m6">
        <div class="card">
            <div class="card-content">
                <span class="card-title">
                    Moje aktualne rezerwacje
                    <div style="display: inline-block; float: right;">
                        <a class="dropdown-button no-print" href="#" data-activates="dropdownMenu" data-alignment="right"><i class="material-icons">&#xE5D4;</i></a>
                        <ul id="dropdownMenu" class="dropdown-content">
                            <li class="no-print"><a onclick="javascript:window.print();">Drukuj</a></li>
                        </ul>
                    </div>
                </span>
            </div>
            <div class="card-action">
                
                <?php echo App\Models\Loader::getMyReservations(); ?>
                
            </div>
        </div>
    </div>
    <div class="col s12 m6 no-print">
        <div class="card">
            <div class="card-content">
                <span class="card-title">Sprawdź dostępność i zarezerwuj</span>
                <p>Sprawdź dostępność sali w interesującym Cię terminie i dokonaj rezerwacji.</p>
            </div>
            <div class="card-action">
                <div class="input-field col s12">
                    <div class="input-field col s12">
                        <select name="roomReservationSelect" id="roomReservationSelect">
                            <option value="" disabled selected>wybierz numer sali</option>
                            
                            <?php echo App\Models\Loader::getRoomsList(); ?>
                            
                        </select>                    
                    </div>
                    <div class="input-field col s12">
                        <input id="dateTimeStart" type="text" class="dateTime validate" required />
                        <label for="dateTimeStart">czas rozpoczęcia</label>
                    </div>
                    <div class="input-field col s12">
                        <input id="dateTimeEnd" type="text" class="dateTime validate" required />
                        <label for="dateTimeEnd">czas zakończenia</label>
                    </div>
                </div>
                <div class="right-align">
                    <button class="waves-effect waves-light btn green" type="button" onclick="getRoomCheck('roomReservationSelect', 'dateTimeStart', 'dateTimeEnd', '<?php getSubDomain(); ?>')">SPRAWDŹ</button>
                </div>
            </div>
        </div>
    </div>
</div>
