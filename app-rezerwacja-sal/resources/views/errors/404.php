<!DOCTYPE html>
<html lang="pl">
    <head>
        <title>404 - System Rezerwacji Sal na Wydziale Chemii</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="x-ua-compatible" content="IE=edge">
        <meta name="theme-color" content="#0d47a1">
        <meta name="description" content="Aplikacja webowa Wydziału Chemii Uniwersytetu Wrocławskiego do rezerwacji sal.">
        <link rel="icon" type="image/png" href="<?php asset("favicon.png"); ?>">
        <link rel="manifest" href="<?php asset("manifest.json"); ?>">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="<?php asset("css/materialize.min.css"); ?>">
        <link rel="stylesheet" href="<?php asset("css/style.min.css"); ?>">
        <link rel="stylesheet" href="<?php asset("css/jquery.datetimepicker.min.css"); ?>">
    </head>
    <body>
        <div class="main">            
            <div class="row" style="margin-top: 50px;">
                <div class="col s12 offset-m3 m6">
                    <div class="card red darken-3 white-text">
                        <div class="card-content center">
                            <h5>Podana strona nie istnieje!</h5>
                        </div>
                    </div>
                </div>
              </div>            
        </div>        

        <footer class="page-footer blue darken-1">
            <div class="container">
                <div class="row">
                    <div class="col l6 s12 white-text">
                        <div>System Rezerwacji Sal</div>
                        <div>Wydział Chemii Uniwersytetu Wrocławskiego</div>
                        <div>ul. F. Joliot-Curie 14</div>
                        <div>50-383 Wrocław</div>
                    </div>
                </div>
            </div>
            <div class="footer-copyright">
                <div class="container">
                Created by <a href="mailto:info@tomaszmiller.pl" class="green-text text-lighten-3">Tomasz Miller</a> with the power of <a class="green-text text-lighten-3" href="http://materializecss.com">Materialize</a>, <a href="https://material.io/icons/" class="green-text text-lighten-3">Material icons</a> and <a href="http://php.net/" class="green-text text-lighten-3">PHP</a> | Wroclaw 2017 
                </div>
            </div>
        </footer>
        
        <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
        <script src="<?php asset("js/materialize.min.js"); ?>"></script>
        <script src="<?php asset("js/scripts.min.js"); ?>"></script>
    </body>
</html>