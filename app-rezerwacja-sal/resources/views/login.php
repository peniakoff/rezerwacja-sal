<!DOCTYPE html>
<html lang="pl">
    <head>
        <title>Logowanie do Systemu Rezerwacji Sal na Wydziale Chemii</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="x-ua-compatible" content="IE=edge">
        <meta name="theme-color" content="#0d47a1">
        <meta name="description" content="Aplikacja webowa Wydziału Chemii Uniwersytetu Wrocławskiego do rezerwacji sal.">
        <link rel="icon" type="image/png" href="<?php asset("favicon.png"); ?>">
        <link rel="manifest" href="<?php asset("manifest.json"); ?>">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="<?php asset("css/materialize.min.css"); ?>">
        <link rel="stylesheet" href="<?php asset("css/style.min.css"); ?>">
        <link rel="stylesheet" href="<?php asset("css/jquery.datetimepicker.min.css"); ?>">
    </head>
    <body>

        <div class="main">

            <!-- Navbar -->
            <nav class="blue darken-3" role="navigation">
                <div class="nav-wrapper container">
                    <a id="logo-container" href="<?php asset(""); ?>" class="hide-on-small-only" title="System Rezerwacji Sal na Wydziale Chemii">System Rezerwacji Sal na Wydziale Chemii</a>
                    <a id="logo-container" href="<?php asset(""); ?>" class="hide-on-med-and-up" title="System Rezerwacji Sal na Wydziale Chemii">System Rezerwacji Sal</a>
                </div>
            </nav>
                
            <?php App\Models\Action::loginMessage(); ?>

            <div class="row">
                <div class="col s12 offset-m3 m6 offset-l4 l4">
                    <div class="card">
                        <div class="card-content">
                            <span class="card-title">Logowanie</span>
                        </div>
                        <div class="card-action">                                
                            <form method="POST">
                                <div class="input-field">
                                    <input name="email" id="email" type="text" class="validate" required autofocus>
                                    <label for="email">Adres e-mail</label>
                                </div>
                                <div class="input-field">
                                    <input name="password" id="last_name" type="password" class="validate" required>
                                    <label for="password">Hasło</label>
                                </div>
                                 <div class="right-align">
                                    <input class="waves-effect waves-light btn green" type="submit" value="ZALOGUJ" />
                                </div>
                             </form>                                
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <footer class="page-footer blue darken-1">
            <div class="container">
                <div class="row">
                    <div class="col l6 s12 white-text">
                        <div>System Rezerwacji Sal</div>
                        <div>Wydział Chemii Uniwersytetu Wrocławskiego</div>
                        <div>ul. F. Joliot-Curie 14</div>
                        <div>50-383 Wrocław</div>
                    </div>
                </div>
            </div>
            <div class="footer-copyright">
                <div class="container">
                Created by <a href="mailto:info@tomaszmiller.pl" class="green-text text-lighten-3">Tomasz Miller</a> with the power of <a class="green-text text-lighten-3" href="http://materializecss.com">Materialize</a>, <a href="https://material.io/icons/" class="green-text text-lighten-3">Material icons</a> and <a href="http://php.net/" class="green-text text-lighten-3">PHP</a> | Wroclaw 2017 
                </div>
            </div>
        </footer>

        <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
        <script src="<?php asset("js/materialize.min.js"); ?>"></script>
        <script src="<?php asset("js/scripts.min.js"); ?>"></script>
    </body>
</html>
