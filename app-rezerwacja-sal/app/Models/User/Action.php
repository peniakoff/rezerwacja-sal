<?php


namespace App\Models\User;

/**
 * Description of Action
 *
 * @author Tomasz Miller
 */
class Action {
    
    public static function checkRoomReservation($array) {
        global $db;
        $startTime = str_replace("%20", " ", $array[2]);
        $endTime = str_replace("%20", " ", $array[3]);
        if ($startTime < $endTime) {
            $sql = "SELECT * FROM `rr_reservations` WHERE `room_id` = {$array[1]} AND ("
                    . "(`start_time` < '{$startTime}' AND `end_time` > '{$startTime}') "
                    . "OR (`start_time` > '{$startTime}' AND `end_time` < '{$endTime}') "
                    . "OR (`start_time`< '{$endTime}' AND `end_time` > '{$endTime}'))";        
            if ($result = $db->query($sql)) {
                if ($result->num_rows > 0) {
                    $_SESSION['info_card'] = [3, "Wybrany termin dla tej sali nie jest dostępny."];
                    redirect(\App\Models\Requests::getWebPageAddress() . '/user');
                } else {
                    $rooms = \App\Models\Action::roomsID();
                    $content = '<div class="row">'
                            . '<div class="col s12"><div class="card">'
                            . '<div class="card-content"><span class="card-title">'
                            . 'Rezerwacja sali</span>'
                            . 'Sala <strong>' . $rooms[$array[1]][0] . '</strong> w terminie'
                            . ' od <strong>' . strtr(substr($startTime, 0, 16), \App\Models\Loader::$TRANS) . '</strong> do <strong>' . strtr(substr($endTime, 0, 16), \App\Models\Loader::$TRANS). '</strong> jest dostępna.'
                            . ' Czy chcesz dokonać teraz jej rezerwacji?</div><div class="card-action right-align">'
                            . '<a href="'
                            . \App\Models\Requests::getWebPageAddress() . '/user/book/' . $array[1] . '/' . $array[2] . '/' . $array[3] 
                            . '" class="waves-effect waves-light btn green">TAK</a>'
                            . ' <a href="'
                            . \App\Models\Requests::getWebPageAddress() . '/user'
                            . '" class="waves-effect waves-light btn red">NIE</a>'
                            . '</div></div></div></div>';
                }
                echo $content;
                $result->close();
            } else {
                $_SESSION['info_card'] = [4, "Wystąpił błąd w połączeniu z bazą danych."];
                redirect(\App\Models\Requests::getWebPageAddress() . '/user');
            }
        } else {
            $_SESSION['info_card'] = [4, "Wprowadzono nieprawidłowe dane."];
            redirect(\App\Models\Requests::getWebPageAddress() . '/user');
        }        
    }
    
    public static function setNewReservation($array) {
        global $db;
        $array[2] = str_replace("%20", " ", $array[2]);
        $array[3] = str_replace("%20", " ", $array[3]);
        if ($array[2] < $array[3]) {
            $sql = "SELECT * FROM `rr_reservations` WHERE `room_id` = {$array[1]} AND ("
                    . "(`start_time` < '{$array[2]}' AND `end_time` > '{$array[2]}') "
                    . "OR (`start_time` > '{$array[2]}' AND `end_time` < '{$array[3]}') "
                    . "OR (`start_time`< '{$array[3]}' AND `end_time` > '{$array[3]}'))"; 
            if ($result = $db->query($sql)) {
                if ($result->num_rows > 0) {
                    $_SESSION['info_card'] = [3, "Wywołany termin rezerwacji dla tej sali jest już zajęty"];
                } else {
                    $sql  = "INSERT INTO `rr_reservations`"
                            . " (`id`, `user_id`, `room_id`, `start_time`, `end_time`, `reservation_time`)"
                            . " VALUES (NULL, {$_SESSION['userId']}, {$array[1]}, '{$array[2]}', '{$array[3]}', CURRENT_TIMESTAMP)";
                    if ($db->query($sql)) {
                        $_SESSION['info_card'] = [2, "Rezerwacja została pomyślnie dokonana."];
                    } else {
                        $_SESSION['info_card'] = [4, "Wystąpił błąd podczas wykonywania rezerwacji."];
                    }
                }
                $result->close();
            } else {
                $_SESSION['info_card'] = [4, "Wystąpił błąd w połączeniu z bazą danych."];
            }
        } else {
            $_SESSION['info_card'] = [4, "Wprowadzono nieprawidłowe dane."];
        }
        redirect(\App\Models\Requests::getWebPageAddress() . '/user');
    }
    
    public static function reservationRemove($array) {
        global $db;
        $sql = "SELECT * FROM `rr_reservations` WHERE "
                . "`id` = {$array[1]} AND `user_id` = {$_SESSION['userId']} LIMIT 1";
        if ($result = $db->query($sql)) {
            if ($result->num_rows > 0) {
                $rooms = \App\Models\Action::roomsID();
                $obj = $result->fetch_object();
                $content = '<div class="row">'
                        . '<div class="col s12"><div class="card">'
                        . '<div class="card-content"><span class="card-title">'
                        . 'Usuwanie rezerwacji</span>'
                        . 'Czy chcesz usunąć rezerwację sali <strong>'
                        . $rooms[$obj->room_id][0]
                        . '</strong> w terminie <strong>' . strtr(substr($obj->start_time, 0, 16), \App\Models\Loader::$TRANS)
                        . ' - ' . strtr(substr($obj->end_time, 0, 16), \App\Models\Loader::$TRANS)
                        . '</strong>? Tej operacji nie można cofnąć.</div><div class="card-action right-align">'
                        . '<a href="'
                        . \App\Models\Requests::getWebPageAddress() . '/user/remove/' . $array[1] . '/confirm' 
                        . '" class="waves-effect waves-light btn green">TAK</a>'
                        . ' <a href="'
                        . \App\Models\Requests::getWebPageAddress() . '/user'
                        . '" class="waves-effect waves-light btn red">NIE</a>'
                        . '</div></div></div></div>';
            } else {
                $_SESSION['info_card'] = [3, "Brak rezerwacji do usunięcia."];
                redirect(\App\Models\Requests::getWebPageAddress() . '/user');
            }
            $result->close();
        } else {
            $_SESSION['info_card'] = [4, "Wystąpił błąd w połączeniu z bazą danych."];
            redirect(\App\Models\Requests::getWebPageAddress() . '/user');
        }
        return $content;
    }

    public static function confirmReservationRemove($array) {
        global $db;
        $sql = "SELECT * FROM `rr_reservations` WHERE "
                . "`id` = {$array[1]} AND `user_id` = {$_SESSION['userId']} LIMIT 1";
        if ($result = $db->query($sql)) {
            if ($result->num_rows > 0) {
                $sql  = "DELETE FROM `rr_reservations` WHERE "
                        . "`id` = {$array[1]} AND `user_id` = {$_SESSION['userId']}";
                if ($db->query($sql)) {
                    $_SESSION['info_card'] = [2, "Rezerwacja została pomyślnie usunięta."];
                } else {
                    $_SESSION['info_card'] = [4, "Wystąpił błąd podczas usuwania rezerwacji."];
                }
            } else {
                $_SESSION['info_card'] = [3, "Brak rezerwacji odniesienia."];
            }
            $result->close();
        } else {
            $_SESSION['info_card'] = [4, "Wystąpił błąd w połączeniu z bazą danych."];
        }        
        redirect(\App\Models\Requests::getWebPageAddress() . '/user');
    }
    
}
