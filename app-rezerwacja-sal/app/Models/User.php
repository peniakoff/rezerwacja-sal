<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Models;

/**
 * Description of User
 *
 * @author Tomasz Miller
 */

class User {

    private $id;
    private $email;
    private $role;
    private static $instance;
    
    public static function getInstance($id, $email, $role) {
        if(self::$instance == null) self::$instance = new User($id, $email, $role);
         return self::$instance;
   }
        
    private function __construct($id, $email, $role) {
        $this->id = $id;
        $this->email = $email;
        $this->role = $role;
    }

    public function getID() {
        return $this->id;
    }
    
    public function getEmail() {
        return $this->email;
    }
            
    public function getRole() {
        return $this->role;
    }
    
    private static function createUser($email) {
        global $db;
        $sql = "INSERT INTO `rr_users` (`id`, `email`, `reg_date`, `role`) VALUES (NULL, '{$email}', CURRENT_TIMESTAMP, '0')";
        if ($db->query($sql)) {
            $user = self::authUser($email);
            return $user;
        }
    }
    
    private static function authUser($email) {
        global $db;
        $userEmail = $db->escapeString($email);
        $sql = "SELECT * FROM `rr_users` WHERE `email`='{$userEmail}' LIMIT 1";        
        $result = $db->query($sql);
        if ($result->num_rows > 0) {
            $obj = $result->fetch_object();
            $user = new User($obj->id, $obj->email, $obj->role);
            return $user;
        }       
    }

    public static function verifyUser($email, $password) {
        global $db;
//        $radius = new \App\Models\Radius('156.17.103.1', 'mNWHHn4TMeP8LXubU8Qctyx2MXwnfePp');
//        $radius->SetNasIpAddress('127.0.0.1');
//        $radius->SetNasPort(0);
//        if (!$radius->AccessRequest($email, $password)) {
//            return FALSE;
//        }
        $sql = "SELECT * FROM `rr_users` WHERE `email`='{$email}' LIMIT 1";        
        $result = $db->query($sql);
        if ($result->num_rows > 0) {
            $user = self::authUser($email);
            return $user;
        } else {
            $user = self::createUser($email);
            return $user;
        }
        return FALSE;
    }
    
}
