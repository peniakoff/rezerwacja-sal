<?php

namespace App\Models;

/**
 * Description of Routes
 *
 * @author Tomasz Miller
 */
class Routes {
    
    public static function user() {
        global $session;
        if (!$session->isSignedIn()) {
            redirect(Requests::getWebPageAddress() . '/login');
        }
    }
    
    public static function login() {
        global $session;
        if ($session->isSignedIn()) {
            redirect("user");
        }
        if (isset($_POST['email']) && isset($_POST['password'])) {
            $message = '';
            $email = htmlentities(trim($_POST['email']), ENT_NOQUOTES);
            $password = htmlentities(trim($_POST['password']), ENT_NOQUOTES);
            do {
                if (!filter_var($email, FILTER_VALIDATE_EMAIL) || !preg_match('/[a-zA-Z0-9.]+@chem.uni.wroc.pl/', $email)) {
                    $_SESSION['message'] = 'Podany adres e-mail jest nieprawidłowy!';
                    break;
                }
                if ($userFound = User::verifyUser($email, $password)) {
                    $session->login($userFound);
                    redirect(Requests::getWebPageAddress() . '/user');
                } else {
                    $_SESSION['message'] = 'Podany adres e-mail lub hasło są nieprawidłowe!';
                    break;
                }        
            } while (0);
        }
    }
    
}
