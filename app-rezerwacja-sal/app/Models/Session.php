<?php

namespace App\Models;

/**
 * Description of Session
 *
 * @author Tomasz Miller
 */

class Session {
    
    private $signedIn = false;
    public $userId;

    function __construct() {
        session_start();
        $this->checkTheLogin();
    }
    
    private function checkTheLogin() {
        if (isset($_SESSION['userId'])) {
            $this->userId = $_SESSION['userId'];
            $this->signedIn = true;
        } else {
            unset($this->userId);
            $this->signedIn = false;
        }
    }
    
    public function isSignedIn() {
        return $this->signedIn;
    }
    
    public function login($user) {
        if ($user) {
            $this->userId = $_SESSION['userId'] = $user->getID();
            $_SESSION['user'] = $user->getEmail();
            $this->signedIn = true;
        }
    }
    
    public function logout() {
        unset($_SESSION['userId']);
        unset($this->userId);
        $this->signedIn = false;
        session_destroy();
    }
    
}
