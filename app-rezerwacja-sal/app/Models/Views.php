<?php

/**
 * Description of Views
 *
 * @author Tomasz Miller
 */

namespace App\Models;

class Views {

    public static $viewArray;

    public function __construct($viewArray) {        
        self::$viewArray = $viewArray;        
        if (isset(self::$viewArray[Requests::getView()])) {
            $this->getContent(Requests::getView());
        } else {
            $this->getError('404');
        }
    }
    
    private function getContent($view) {        
        if (sizeof(self::$viewArray[$view]) > 1) {
            $content = __DIR__ . '/../../resources/views/' . self::$viewArray[$view][0] . '/' . self::$viewArray[$view][1] . '.php';
            require __DIR__ . '/../../resources/views/layout/default.php';
        } else {
            require __DIR__ . '/../../resources/views/' . self::$viewArray[$view][0] . '.php';
        }        
    }
    
    private function getError($error) {        
        require __DIR__ . '/../../resources/views/errors/' . $error . '.php';        
    }
    
}
