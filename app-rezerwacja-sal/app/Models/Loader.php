<?php

namespace App\Models;

/**
 * Description of Loader
 *
 * @author Peniakoff
 */

class Loader {

    public static $TRANS = ["-" => ".", " " => ", godz. "];

    public static $roomIDs;

    public static function getMenu() {
        global $session;
        $content = "";
        if ($session->isSignedIn()) {
            $content .= '<li><a class="dropdown-button" href="#!" data-activates="dropdown1" data-beloworigin="true"><i class="material-icons right">&#xE5CF;</i>';
            $content .= '<span class="truncate user-name">';
            $content .= $_SESSION['user'];
            $content .= '</span></a>';
            $content .= '<ul id="dropdown1" class="dropdown-content"><li><a href="';
            $content .= Requests::getWebPageAddress() . '/user';
            $content .= '">Moje rezerwacje</a></li><li><a href="';
            $content .= Requests::getWebPageAddress() . '/user/logout';
            $content .= '" title="kliknij, aby się wylogować">Wyloguj</a></li></ul></li>';
        } else {
            $content .= '<li><a href="';
            $content .= Requests::getWebPageAddress() . '/login';
            $content .= '" title="zaloguj się, aby dokonać rezerwacji"><i class="material-icons left">&#xE890;</i>Zaloguj</a></li>';
        }
        return $content;
    }
    
    public static function getMenuMobile() {
        global $session;
        $content = "";
        if ($session->isSignedIn()) {
            $content .= '<li><a href="';
            $content .= Requests::getWebPageAddress() . '/user';
            $content .= '">Moje rezerwacje</a></li><li><a href="';
            $content .= Requests::getWebPageAddress() . '/user/logout';
            $content .= '" title="kliknij, aby się wylogować">Wyloguj</a></li>';
        } else {
            $content .= '<li><a href="';
            $content .= Requests::getWebPageAddress() . '/login';
            $content .= '" title="zaloguj się, aby dokonać rezerwacji">Zaloguj</a></li>';
        }
        return $content;
    }

    public static function getRoomsIDs() {
        global $db;
        $sql = 'SELECT * FROM `rr_rooms` ORDER BY `room_number` ASC';
        if ($query = $db->query($sql)) {
            while ($obj = $query->fetch_object()) {
                self::$roomIDs[$obj->id] = [$obj->room_number, $obj->room_seats, $obj->type];
            }
            $query->close();
        }
    }    
    
    public static function getRoomsList() {
        if (is_null(self::$roomIDs)) {
            self::getRoomsIDs();
        }
        $list = '';
        $array = self::$roomIDs;
        foreach ($array as $key => $value) {
            $list .= '<option value="' . $key . '">' . $value[0] . '</option>';
        }        
        return $list;
    }
    
    public static function getLastReservations() {
        global $db;
        $sql = 'SELECT * FROM `rr_reservations` WHERE `start_time` >= CURDATE() ORDER BY `id` DESC LIMIT 10';        
        if ($result = $db->query($sql)) {            
            if ($result->num_rows > 0) {                
                $table = '<div class="row">'
                        . '<div class="col s12"><div class="card">'
                        . '<div class="card-content"><span class="card-title">'
                        . 'Ostatnio dokonane rezerwacje'
                        . '</span></div><div class="card-action">'
                        . '<table class="striped centered"><thead><tr>'
                        . '<th>Lp.</th><th>Numer sali</th><th>Termin rozpoczęcia</th>'
                        . '<th>Termin zakończenia</th></tr></thead><tbody>';                
                $number = 1;
                while ($obj = $result->fetch_object()) {
                    $table .= '<tr>';
                    $table .= '<td>' . $number . '</td>';
                    $table .= '<td>' . self::$roomIDs[$obj->room_id][0] . '</td>';
                    $table .= '<td>' . strtr(substr($obj->start_time, 0, 16), self::$TRANS) . '</td>';
                    $table .= '<td>' . strtr(substr($obj->end_time, 0, 16), self::$TRANS) . '</td>';
                    $table .= '</tr>';
                    $number ++;
                }
                $table .= '</tbody></table></div></div></div></div>';
                $result->close();
                return $table;
            } else {
                return;
            }
        }
        
    }
    
    public static function getFullRoomListTable() {
        $number = 1; 
        if (is_null(self::$roomIDs)) {
            self::getRoomsIDs();
        }
        $table = '';
        $array = self::$roomIDs;
        foreach ($array as $key => $value) {
            $table .= "<tr>"
                    . "<td>$number</td><td>{$value[0]}</td>"
                    . "<td>{$value[1]}</td>"
                    . "<td>";
            if ($value[2] == 1) {
                $table .= '<i class="material-icons" title="sala komputerowa">&#xE30C;</i>';
            } else {
                $table .= '<i class="material-icons" title="sala seminaryjna/wykładowa">&#xE7EF;</i>';
            }
            $table .= "</td>"
                    . "</tr>";
            $number ++;
        }
        return $table;
    }
    
    public static function getMyReservations() {
        global $db;
        $userID = $_SESSION['userId'];
        $sql = 'SELECT * FROM `rr_reservations` WHERE `user_id` = ' . $userID . ' AND `start_time` >= CURDATE() ORDER BY `start_time` ASC';
        if ($result = $db->query($sql)) {
            if ($result->num_rows > 0) {
                if (is_null(self::$roomIDs)) {
                    self::getRoomsIDs();
                }
                $array = self::$roomIDs;
                $table = '<table class="striped centered"><thead><tr>'
                        . '<th>Lp.</th><th>Numer sali</th><th>Termin rozpoczęcia</th>'
                        . '<th>Termin zakończenia</th><th class="no-print">Opcje</th></tr></thead><tbody>';                
                $number = 1;                
                while ($obj = $result->fetch_object()) {
                    $table .= '<tr>';
                    $table .= '<td>' . $number . '</td>';
                    $table .= '<td>' . $array[$obj->room_id][0] . '</td>';
                    $table .= '<td>' . strtr(substr($obj->start_time, 0, 16), self::$TRANS) . '</td>';
                    $table .= '<td>' . strtr(substr($obj->end_time, 0, 16), self::$TRANS) . '</td>';
                    $table .= '<td class="no-print">'
                            . '<a class="btn-floating waves-effect waves-light red"'
                            . ' href="' . Requests::getWebPageAddress() . '/user/remove/' . $obj->id . '"'
                            . ' title="usuń rezerwację">'
                            . '<i class="material-icons right">&#xE872;</i></a></td>';
                    $table .= '</tr>';
                    $number ++;
                }
                $table .= '</tbody></table>';
                $result->close();
                return $table;
            } else {
                return '<p class="card-panel blue accent-2 white-text center">Brak informacji o przyszłych rezerwacjach</p>';
            }
        }
        
    }
    
    public static function getRoomReservationByID($id) {
        global $db;
        $sql = 'SELECT * FROM `rr_reservations` '
                . 'WHERE `room_id` = ' . $id . ' '
                . 'AND `start_time` >= CURDATE() '
                . 'ORDER BY `start_time` ASC';
        if ($result = $db->query($sql)) {
            if ($result->num_rows > 0) {
                if (is_null(self::$roomIDs)) {
                    self::getRoomsIDs();
                }
                $array = self::$roomIDs;
                $content = '<div class="row">'
                . '<div class="col s12"><div class="card">'
                . '<div class="card-content"><span class="card-title">'
                . 'Status wybranej rezerwacji'
                . '</span></div><div class="card-action">'
                . '<table class="striped centered"><thead><tr>'
                . '<th>Lp.</th><th>Numer sali</th><th>Termin rozpoczęcia</th>'
                . '<th>Termin zakończenia</th></tr></thead><tbody>';                
                $number = 1;
                while ($obj = $result->fetch_object()) {
                    $content .= '<tr>';
                    $content .= '<td>' . $number . '</td>';
                    $content .= '<td><strong>' . $array[$obj->room_id][0] . '</strong></td>';
                    $content .= '<td>' . strtr(substr($obj->start_time, 0, 16), self::$TRANS) . '</td>';
                    $content .= '<td>' . strtr(substr($obj->end_time, 0, 16), self::$TRANS) . '</td>';
                    $content .= '</tr>';
                    $number ++;
                }
                $content .= '</tbody></table></div></div></div></div>';
            } else {
                $content = Action::infoCard(3, "Brak wyników wyszukiwania spełniających podane parametry.");
            }
            $result->close();
        } else {
            $content = Action::infoCard(4, "Wystąpił błąd w połączeniu z bazą danych.");
        }
        return $content;
    }
    
    public static function getRoomReservationByDate($date) {
        global $db;
        $sql = "SELECT * FROM `rr_reservations` WHERE `start_time` "
                . "BETWEEN '{$date} 00:00:00' AND '{$date} 23:59:59' "
                . "ORDER BY `start_time` ASC";
        if ($result = $db->query($sql)) {
            if ($result->num_rows > 0) {
                if (is_null(self::$roomIDs)) {
                    self::getRoomsIDs();
                }
                $array = self::$roomIDs;
                $content = '<div class="row">'
                . '<div class="col s12"><div class="card">'
                . '<div class="card-content"><span class="card-title">'
                . 'Status wybranej rezerwacji'
                . '</span></div><div class="card-action">'
                . '<table class="striped centered"><thead><tr>'
                . '<th>Lp.</th><th>Numer sali</th><th>Termin rozpoczęcia</th>'
                . '<th>Termin zakończenia</th></tr></thead><tbody>';                
                $number = 1;
                while ($obj = $result->fetch_object()) {
                    $content .= '<tr>';
                    $content .= '<td>' . $number . '</td>';
                    $content .= '<td>' . $array[$obj->room_id][0] . '</td>';
                    $content .= '<td><strong>' . strtr(substr($obj->start_time, 0, 16), self::$TRANS) . '</strong></td>';
                    $content .= '<td>' . strtr(substr($obj->end_time, 0, 16), self::$TRANS) . '</td>';
                    $content .= '</tr>';
                    $number ++;
                }
                $content .= '</tbody></table></div></div></div></div>';
            } else {
                $content = Action::infoCard(3, "Brak wyników wyszukiwania spełniających podane parametry.");
            }
            $result->close();
        } else {
            $content = Action::infoCard(4, "Wystąpił błąd w połączeniu z bazą danych.");
        }
        return $content;
    }
    
}
