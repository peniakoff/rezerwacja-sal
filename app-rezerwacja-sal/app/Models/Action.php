<?php

namespace App\Models;

/**
 * Description of Action
 *
 * @author Tomasz Miller
 */
class Action {
    
    public static function roomsID() {
        if (is_null(Loader::$roomIDs)) {
            Loader::getRoomsIDs();
        }
        return Loader::$roomIDs;
    }

    public static function homeAction() {
        if (is_array(Requests::getAction())) {
            $array = Requests::getAction();
            if ($array[0] == 'room') {
                do {
                    if (preg_match('/^[0-9]{1,2}$/', $array[1])) {
                        echo Loader::getRoomReservationByID($array[1]);
                        break;
                    }
                    if (preg_match('/^[0-9]{4}\-[0-1]{1}[0-9]{1}\-[0-3]{1}[0-9]{1}$/', $array[1])) {
                        echo Loader::getRoomReservationByDate($array[1]);
                        break;
                    }
                } while (0);
            }
        }
    }

    public static function userReservationsAction() {
        if (is_array(Requests::getAction())) {
            $array = Requests::getAction();
            if ($array[0] == 'check') {
                do {
                    if (preg_match('/^[0-9]{1,2}$/', $array[1])) {
                        if (preg_match('/^[0-9]{4}\-[0-1]{1}[0-9]{1}\-[0-3]{1}[0-9]{1}\%20[0-2]{1}[0-9]{1}\:[0-5]{1}[0-9]{1}$/', $array[2])) {
                            if (preg_match('/^[0-9]{4}\-[0-1]{1}[0-9]{1}\-[0-3]{1}[0-9]{1}\%20[0-2]{1}[0-9]{1}\:[0-5]{1}[0-9]{1}$/', $array[3])) {
                                echo User\Action::checkRoomReservation($array);
                                break;
                            }
                        }
                        break;
                    }
                    break;
                } while (0);
            }
            if ($array[0] == 'book') {
                do {
                    if (preg_match('/^[0-9]{1,2}$/', $array[1])) {
                        if (preg_match('/^[0-9]{4}\-[0-1]{1}[0-9]{1}\-[0-3]{1}[0-9]{1}\%20[0-2]{1}[0-9]{1}\:[0-5]{1}[0-9]{1}$/', $array[2])) {
                            if (preg_match('/^[0-9]{4}\-[0-1]{1}[0-9]{1}\-[0-3]{1}[0-9]{1}\%20[0-2]{1}[0-9]{1}\:[0-5]{1}[0-9]{1}$/', $array[3])) {
                                User\Action::setNewReservation($array);
                                break;
                            }
                        }
                        break;
                    }
                    break;
                } while (0);
            }
            if ($array[0] == 'remove') {
                if (preg_match('/^[0-9]{1,2}$/', $array[1])) {
                    echo User\Action::reservationRemove($array);
                }
            }
            if (isset($array[2]) && $array[2] == 'confirm' && $array[0] == 'remove') {
                if (preg_match('/^[0-9]{1,2}$/', $array[1])) {
                    User\Action::confirmReservationRemove($array);
                }
            }
        }
    }

    public static function infoCardAction() {
        if (isset($_SESSION['info_card']) && is_array($_SESSION['info_card'])) {
            echo self::infoCard(
                    $_SESSION['info_card'][0], 
                    $_SESSION['info_card'][1]);
            unset($_SESSION['info_card']);
        }
    }

    public static function infoCard($id, $message) {
        $content = '<div class="row no-print">'
                . '<div class="col s12 offset-m1 m10 offset-l2 l8">';
        $content .= self::infoCardSwitch($id);
        $content .= '<span onclick="this.parentElement.parentElement.parentElement.style.display=\'none\'"'
                . 'class="close-info-card">'
                . '<i class="material-icons" title="OK, zamknij tę kartę">&#xE5CD;</i></span>'
                . '<div class="card-content center"><p>';
        $content .= $message;
        $content .= '</p></div></div></div></div>';
        return $content;
    }
    
    private static function infoCardSwitch($id) {
        switch ($id) {
        case 1: //information
            $content = '<div class="card blue accent-2 white-text">';
            break;
        case 2: //success
            $content = '<div class="card green darken-1 white-text">';
            break;
        case 3: //warning
            $content = '<div class="card amber grey-text text-darken-3">';
            break;
        case 4: //danger
            $content = '<div class="card red darken-1 white-text">';
            break;
        }
        return $content;
    }
    
    public static function loginAction() {
        if (is_array(Requests::getAction())) {
        $array = Requests::getAction();
        if ($array[0] == 'logout') {
            global $session;
            $session->logout();
            redirect(Requests::getWebPageAddress());
        }
    }
    }

    public static function loginMessage() {
        if (isset($_SESSION['message'])) {
            echo '<div class="row">'
                    . '<div class="col s12 offset-m3 m6 offset-l4 l4">'
                    . '<div class="card red darken-1 white-text">'
                    . '<span onclick="this.parentElement.style.display=\'none\'"'
                    . 'class="" style="float: right; cursor: pointer;">'
                    . '<i class="material-icons" title="OK, zamknij to ostrzeżenie">&#xE5CD;</i></span>'
                    . '<div class="card-content center"><p>'
                    . $_SESSION['message']
                    . '</p>'
                    . '</div></div></div></div>';
            unset($_SESSION['message']);
        }
    }
    
}
