<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Models;

class Radius {
    /**
     * Private class properties
     */
    private $_ip_radius_server;
    private $_shared_secret;
    private $_radius_suffix;
    private $_udp_timeout;
    private $_authentication_port;
    private $_accounting_port;
    private $_nas_ip_address;
    private $_nas_port;
    private $_encrypted_password;
    private $_user_ip_address;
    private $_request_authenticator;
    private $_response_authenticator;
    private $_username;
    private $_password;
    private $_identifier_to_send;
    private $_identifier_received;
    private $_radius_packet_to_send;
    private $_radius_packet_received;
    private $_attributes_to_send;
    private $_attributes_received;
    private $_socket_to_server;
    private $_attributes_info;
    private $_radius_packet_info;
    private $_last_error_code;
    private $_last_error_message;


    /**
     * Radius class constructor
     *
     * @param string   IP address of the radius server
     * @param string   Shared secret with the radius server
     * @param string   Radius domain name suffix
     * @param integer  UDP timeout
     * @param integer  Radius authentication port
     * @param integer  Radius accounting port
     *
     * @return void
     */
	 
    public function __construct($ip_radius_server = '127.0.0.1', $shared_secret = '', $radius_suffix = '', $udp_timeout = 5, $authentication_port = 1812, $accounting_port = 1813) {
        $this->_radius_packet_info[1] = 'Access-Request';
        $this->_radius_packet_info[2] = 'Access-Accept';
        $this->_radius_packet_info[3] = 'Access-Reject';
        $this->_radius_packet_info[4] = 'Accounting-Request';
        $this->_radius_packet_info[5] = 'Accounting-Response';
        $this->_radius_packet_info[11] = 'Access-Challenge';
        $this->_radius_packet_info[12] = 'Status-Server (experimental)';
        $this->_radius_packet_info[13] = 'Status-Client (experimental)';
        $this->_radius_packet_info[255] = 'Reserved';

        $this->_attributes_info[1] = array('User-Name', 'S');
        $this->_attributes_info[2] = array('User-Password', 'S');
        $this->_attributes_info[3] = array('CHAP-Password', 'S');
        $this->_attributes_info[4] = array('NAS-IP-Address', 'A');
        $this->_attributes_info[5] = array('NAS-Port', 'I');
        $this->_attributes_info[6] = array('Service-Type', 'I');
        $this->_attributes_info[7] = array('Framed-Protocol', 'I');
        $this->_attributes_info[8] = array('Framed-IP-Address', 'A');
        $this->_attributes_info[9] = array('Framed-IP-Netmask', 'A');
        $this->_attributes_info[10] = array('Framed-Routing', 'I');
        $this->_attributes_info[11] = array('Filter-Id', 'T');
        $this->_attributes_info[12] = array('Framed-MTU', 'I');
        $this->_attributes_info[13] = array('Framed-Compression', 'I');
        $this->_attributes_info[14] = array( 'Login-IP-Host', 'A');
        $this->_attributes_info[15] = array('Login-service', 'I');
        $this->_attributes_info[16] = array('Login-TCP-Port', 'I');
        $this->_attributes_info[17] = array('(unassigned)', '');
        $this->_attributes_info[18] = array('Reply-Message', 'T');
        $this->_attributes_info[19] = array('Callback-Number', 'S');
        $this->_attributes_info[20] = array('Callback-Id', 'S');
        $this->_attributes_info[21] = array('(unassigned)', '');
        $this->_attributes_info[22] = array('Framed-Route', 'T');
        $this->_attributes_info[23] = array('Framed-IPX-Network', 'I');
        $this->_attributes_info[24] = array('State', 'S');
        $this->_attributes_info[25] = array('Class', 'S');
        $this->_attributes_info[26] = array('Vendor-Specific', 'S');
        $this->_attributes_info[27] = array('Session-Timeout', 'I');
        $this->_attributes_info[28] = array('Idle-Timeout', 'I');
        $this->_attributes_info[29] = array('Termination-Action', 'I');
        $this->_attributes_info[30] = array('Called-Station-Id', 'S');
        $this->_attributes_info[31] = array('Calling-Station-Id', 'S');
        $this->_attributes_info[32] = array('NAS-Identifier', 'S');
        $this->_attributes_info[33] = array('Proxy-State', 'S');
        $this->_attributes_info[34] = array('Login-LAT-Service', 'S');
        $this->_attributes_info[35] = array('Login-LAT-Node', 'S');
        $this->_attributes_info[36] = array('Login-LAT-Group', 'S');
        $this->_attributes_info[37] = array('Framed-AppleTalk-Link', 'I');
        $this->_attributes_info[38] = array('Framed-AppleTalk-Network', 'I');
        $this->_attributes_info[39] = array('Framed-AppleTalk-Zone', 'S');
        $this->_attributes_info[60] = array('CHAP-Challenge', 'S');
        $this->_attributes_info[61] = array('NAS-Port-Type', 'I');
        $this->_attributes_info[62] = array('Port-Limit', 'I');
        $this->_attributes_info[63] = array('Login-LAT-Port', 'S');
        $this->_attributes_info[76] = array('Prompt', 'I');

        $this->_identifier_to_send = 0;
        $this->_user_ip_address = (isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '0.0.0.0');

        $this->GenerateRequestAuthenticator();
        $this->SetIpRadiusServer($ip_radius_server);
        $this->SetSharedSecret($shared_secret);
        $this->SetAuthenticationPort($authentication_port);
        $this->SetAccountingPort($accounting_port);
        $this->SetRadiusSuffix($radius_suffix);
        $this->SetUdpTimeout($udp_timeout);
        $this->SetUsername();
        $this->SetPassword();
        $this->SetNasIpAddress();
        $this->SetNasPort();

        $this->ClearLastError();
        $this->ClearDataToSend();
        $this->ClearDataReceived();
    }

    public function GetNextIdentifier() {
        $this->_identifier_to_send = (($this->_identifier_to_send + 1) % 256);
        return $this->_identifier_to_send;
    }

    public function GenerateRequestAuthenticator() {
        $this->_request_authenticator = '';
        for ($ra_loop = 0; $ra_loop <= 15; $ra_loop++) {
            $this->_request_authenticator .= chr(rand(1, 255));
        }
    }

    public function GetRequestAuthenticator() {
        return $this->_request_authenticator;
    }

    public function GetLastError() {
        if ($this->_last_error_code > 0) {
            return $this->_last_error_message . ' (' . $this->_last_error_code . ')';
        }
        else {
            return '';
        }
    }

    public function ClearDataToSend() {
        $this->_radius_packet_to_send = 0;
        $this->_attributes_to_send = null;
    }

    public function ClearDataReceived() {
        $this->_radius_packet_received = 0;
        $this->_attributes_received = null;
    }

    public function SetPacketCodeToSend($packet_code) {
        $this->_radius_packet_to_send = $packet_code;
    }

    public function SetIpRadiusServer($ip_radius_server) {
        $this->_ip_radius_server = gethostbyname($ip_radius_server);
    }

    public function SetSharedSecret($shared_secret) {
        $this->_shared_secret = $shared_secret;
    }


    public function SetRadiusSuffix($radius_suffix) {
        $this->_radius_suffix = $radius_suffix;
    }

    public function SetUsername($username = '') {
        $temp_username = $username;
        if (strpos($temp_username, '@') === false) {
            $temp_username .= $this->_radius_suffix;
        }

        $this->_username = $temp_username;
        $this->SetAttribute(1, $this->_username);
    }

    public function SetPassword($password = '') {
        $this->_password = $password;
        $encrypted_password = '';
        $padded_password = $password;

        if ((strlen($password) % 16) != 0) {
            $padded_password .= str_repeat(chr(0), (16-strlen($password) % 16));
        }

        $previous_result = $this->_request_authenticator;

        for ($full_loop = 0; $full_loop < (strlen($padded_password) / 16); $full_loop++) {
            $xor_value = md5($this->_shared_secret . $previous_result);

            $previous_result = '';
            for ($xor_loop = 0; $xor_loop <= 15; $xor_loop++) {
                $value1 = ord(substr($padded_password, ($full_loop * 16) + $xor_loop, 1));
                $value2 = hexdec(substr($xor_value, 2 * $xor_loop, 2));
                $xor_result = $value1 ^ $value2;
                $previous_result .= chr($xor_result);
            }
            $encrypted_password .= $previous_result;
        }

        $this->_encrypted_password = $encrypted_password;
        $this->SetAttribute(2, $this->_encrypted_password);
    }

    public function SetNasIPAddress($nas_ip_address = '') {
        if (strlen($nas_ip_address) > 0) {
            $this->_nas_ip_address = gethostbyname($nas_ip_address);
        }
        else {
            $this->_nas_ip_address = gethostbyname(isset($_SERVER['SERVER_ADDR']) ? $_SERVER['SERVER_ADDR'] : '0.0.0.0');
        }

        $this->SetAttribute(4, $this->_nas_ip_address);
    }

    public function SetNasPort($nas_port = 0) {
        $this->_nas_port = intval($nas_port);
        $this->SetAttribute(5, $this->_nas_port);
    }

    public function SetUdpTimeout($udp_timeout = 5) {
        if (intval($udp_timeout) > 0) {
            $this->_udp_timeout = intval($udp_timeout);
        }
    }

    public function ClearLastError() {
        $this->_last_error_code = 0;
        $this->_last_error_message = '';
    }

    public function SetAuthenticationPort($authentication_port) {
        if ((intval($authentication_port) > 0) && (intval($authentication_port) < 65536)) {
            $this->_authentication_port = intval($authentication_port);
        }
    }

    public function SetAccountingPort($accounting_port) {
        if ((intval($accounting_port) > 0) && (intval($accounting_port) < 65536)) {
            $this->_accounting_port = intval($accounting_port);
        }
    }

    public function GetReceivedPacket() {
        return $this->_radius_packet_received;
    }

    public function GetReceivedAttributes() {
        return $this->_attributes_received;
    }

    public function GetReadableReceivedAttributes() {
        $readable_attributes = '';

        if (isset($this->_attributes_received)) {
            foreach ($this->_attributes_received as $one_received_attribute) {
                $attributes_info = $this->GetAttributesInfo($one_received_attribute[0]);
                $readable_attributes .= $attributes_info[0] . ': ';

                if ($one_received_attribute[0] == 26) {
                    $vendor_array = $this->DecodeVendorSpecificContent($one_received_attribute[1]);
                    foreach ($vendor_array as $vendor_one) {
                        $readable_attributes .= 'Vendor-Id: ' . $vendor_one[0] . ', Vendor-type: ' . $vendor_one[1] . ',  Attribute-specific: ' . $vendor_one[2];
                    }
                }
                else {
                    $readable_attributes .= $one_received_attribute[1];
                }

                $readable_attributes .= "<br />\n";
            }
        }

        return $readable_attributes;
    }

    public function GetAttribute($attribute_type) {
        $attribute_value = null;
        foreach ($this->_attributes_received as $one_received_attribute) {
            if (intval($attribute_type) == $one_received_attribute[0]) {
                $attribute_value = $one_received_attribute[1];
                break;
            }
        }

        return $attribute_value;
    }

    public function GetRadiusPacketInfo($info_index) {
        if (isset($this->_radius_packet_info[intval($info_index)])) {
            return $this->_radius_packet_info[intval($info_index)];
        }
        else {
            return '';
        }
    }

    public function GetAttributesInfo($info_index) {
        if (isset($this->_attributes_info[intval($info_index)])) {
            return $this->_attributes_info[intval($info_index)];
        }
        else {
            return array('','');
        }
    }

    public function SetAttribute($type, $value) {
        $attribute_index = -1;
        for ($attributes_loop = 0; $attributes_loop < count($this->_attributes_to_send); $attributes_loop++) {
            if ($type == ord(substr($this->_attributes_to_send[$attributes_loop], 0, 1))) {
                $attribute_index = $attributes_loop;
                break;
            }
        }

        $temp_attribute = null;

        if (isset($this->_attributes_info[$type])) {
            switch ($this->_attributes_info[$type][1]) {
            case 'T':
                $temp_attribute = chr($type) .
                    chr(2 + strlen($value)) .
                    $value;
                break;
            case 'S':
                $temp_attribute = chr($type) .
                    chr(2 + strlen($value)) .
                    $value;
                break;
            case 'A':
                $ip_array = explode('.', $value);
                $temp_attribute = chr($type) .
                    chr(6) .
                    chr($ip_array[0]) .
                    chr($ip_array[1]) .
                    chr($ip_array[2]) .
                    chr($ip_array[3]);
                break;
            case 'I':
                $temp_attribute = chr($type) .
                    chr(6) .
                    chr(($value / (256 * 256 * 256)) % 256) .
                    chr(($value / (256 * 256)) % 256) .
                    chr(($value / (256)) % 256) .
                    chr($value % 256);
                break;
            case 'D':
                $temp_attribute = null;
                break;
            default:
                $temp_attribute = null;
            }
        }

        if ($attribute_index > -1) {
            $this->_attributes_to_send[$attribute_index] = $temp_attribute;
        }
        else {
            $this->_attributes_to_send[] = $temp_attribute;
        }

        $attribute_info = $this->GetAttributesInfo($type);
    }

    public function DecodeAttribute($attribute_raw_value, $attribute_format) {
        $attribute_value = null;

        if (isset($this->_attributes_info[$attribute_format])) {
            switch ($this->_attributes_info[$attribute_format][1]) {
            case 'T':
                $attribute_value = $attribute_raw_value;
                break;
            case 'S':
                $attribute_value = $attribute_raw_value;
                break;
            case 'A':
                $attribute_value = ord(substr($attribute_raw_value, 0, 1)) . '.' .
                    ord(substr($attribute_raw_value, 1, 1)) . '.' .
                    ord(substr($attribute_raw_value, 2, 1)) . '.' .
                    ord(substr($attribute_raw_value, 3, 1));
                break;
            case 'I':
                $attribute_value = (ord(substr($attribute_raw_value, 0, 1)) * 256 * 256 * 256) +
                    (ord(substr($attribute_raw_value, 1, 1)) * 256 * 256) +
                    (ord(substr($attribute_raw_value, 2, 1)) * 256) +
                    ord(substr($attribute_raw_value, 3, 1));
                break;
            case 'D':
                $attribute_value = null;
                break;
            default:
                    $attribute_value = null;
            }
        }

        return $attribute_value;
    }

    public function DecodeVendorSpecificContent($vendor_specific_raw_value) {
        $result = array();
        $offset_in_raw = 0;
        $vendor_id = (ord(substr($vendor_specific_raw_value, 0, 1)) * 256 * 256 * 256) +
            (ord(substr($vendor_specific_raw_value, 1, 1)) * 256 * 256) +
            (ord(substr($vendor_specific_raw_value, 2, 1)) * 256) +
             ord(substr($vendor_specific_raw_value, 3, 1));
        $offset_in_raw += 4;

        while ($offset_in_raw < strlen($vendor_specific_raw_value)) {
            $vendor_type = (ord(substr($vendor_specific_raw_value, $offset_in_raw + 0, 1)));
            $vendor_length = (ord(substr($vendor_specific_raw_value, $offset_in_raw + 1, 1)));
            $attribute_specific = substr($vendor_specific_raw_value, $offset_in_raw + 2, $vendor_length);
            $result[] = array($vendor_id, $vendor_type, $attribute_specific);
            $offset_in_raw += ($vendor_length);
        }

        return $result;
    }

    public function AccessRequest($username = '', $password = '', $udp_timeout = 0, $state = null) {
        $this->ClearDataReceived();
        $this->ClearLastError();
        $this->SetPacketCodeToSend(1);

        if (strlen($username) > 0) {
            $this->SetUsername($username);
        }

        if (strlen($password) > 0) {
            $this->SetPassword($password);
        }

        if ($state !== null) {
            $this->SetAttribute(24, $state);
        }
        else {
            $this->SetAttribute(6, 1);
        }

        if (intval($udp_timeout) > 0) {
            $this->SetUdpTimeout($udp_timeout);
        }

        $attributes_content = '';

        for ($attributes_loop = 0; $attributes_loop < count($this->_attributes_to_send); $attributes_loop++) {
            $attributes_content .= $this->_attributes_to_send[$attributes_loop];
        }

        $packet_length  = 4;
        $packet_length += strlen($this->_request_authenticator);
        $packet_length += strlen($attributes_content);

        $packet_data  = chr($this->_radius_packet_to_send);
        $packet_data .= chr($this->GetNextIdentifier());
        $packet_data .= chr(intval($packet_length / 256));
        $packet_data .= chr(intval($packet_length % 256));
        $packet_data .= $this->_request_authenticator;
        $packet_data .= $attributes_content;

        $_socket_to_server = socket_create(AF_INET, SOCK_DGRAM, 17);

        if ($_socket_to_server === false) {
            $this->_last_error_code = socket_last_error();
            $this->_last_error_message = socket_strerror($this->_last_error_code);
        }
        else if (socket_connect($_socket_to_server, $this->_ip_radius_server, $this->_authentication_port) === false) {
            $this->_last_error_code = socket_last_error();
            $this->_last_error_message = socket_strerror($this->_last_error_code);
        }
        else if (socket_write($_socket_to_server, $packet_data, $packet_length) === false) {
            $this->_last_error_code = socket_last_error();
            $this->_last_error_message = socket_strerror($this->_last_error_code);
        }
        else {
            $read_socket_array = array($_socket_to_server);
            $write_socket_array = null;
            $except_socket_array = null;
            $received_packet = chr(0);

            if (!(socket_select($read_socket_array, $write_socket_array, $except_socket_array, $this->_udp_timeout) === false)) {
                if (in_array($_socket_to_server, $read_socket_array)) {
                    if (($received_packet = @socket_read($_socket_to_server, 1024)) === false) {
                        $received_packet = chr(0);
                        $this->_last_error_code = socket_last_error();
                        $this->_last_error_message = socket_strerror($this->_last_error_code);
                    }
                    else {
                        socket_close($_socket_to_server);
                    }
                }
            }
            else {
                socket_close($_socket_to_server);
            }
        }

        $this->_radius_packet_received = intval(ord(substr($received_packet, 0, 1)));

        if ($this->_radius_packet_received > 0) {
            $this->_identifier_received = intval(ord(substr($received_packet, 1, 1)));
            $packet_length = (intval(ord(substr($received_packet, 2, 1))) * 256) + (intval(ord(substr($received_packet, 3, 1))));
            $this->_response_authenticator = substr($received_packet, 4, 16);
            $attributes_content = substr($received_packet, 20, ($packet_length - 4 - 16));

            while (strlen($attributes_content) > 2) {
                $attribute_type = intval(ord(substr($attributes_content, 0, 1)));
                $attribute_length = intval(ord(substr($attributes_content, 1, 1)));
                $attribute_raw_value = substr($attributes_content, 2, $attribute_length - 2);
                $attributes_content = substr($attributes_content, $attribute_length);

                $attribute_value = $this->DecodeAttribute($attribute_raw_value, $attribute_type);
                $attribute_info = $this->GetAttributesInfo($attribute_type);

                $this->_attributes_received[] = array($attribute_type, $attribute_value);
            }
        }

        return (($this->_radius_packet_received) == 2);
    }

}
