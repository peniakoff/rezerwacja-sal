<?php

/**
 * Description of Request
 *
 * @author Tomasz Miller
 */

namespace App\Models;

class Requests {
    
    private $address;
    private $addressArray;
    private $subDomain;
    private $view;
    private $action;

    public function __construct() {        
        $this->address = $_SERVER['REQUEST_URI'];
        $this->addressToArray();
        $this->domainChecker();
        $this->setView();
        $this->setAction();
    }
    
    private function addressToArray() {        
        $addressArray = explode('/', $this->address);
        $this->addressArray = $addressArray;
    }

    private function domainChecker() {
        if (isset($this->subDomain)) {
            return;
        }
        $array = $this->addressArray;
        array_shift($array);
        if (sizeof($array) > 1) {
            $this->subDomain = $array[0];
        } else {
            $this->subDomain = '';
        }
        
    }
    
    private function setView() {
        $array = $this->addressArray;
        array_shift($array);
        if ($array[0] == $this->subDomain) {
            $this->view = $array[1];
        } else {
            $this->view = $array[0];
        }
    }
    
    private function setAction() {
        $array = $this->addressArray;
        array_shift($array);
        if ($array[count($array) - 1] == $this->view) {
            $this->action = '';
        } else {
            if ($array[0] == $this->subDomain) {
                array_shift($array);
            }
            if ($array[0] == $this->view) {
                array_shift($array);
            }
            $this->action = $array;
        }
    }

    private static function isSecure() {
        return (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') || $_SERVER['SERVER_PORT'] == 443;
    }
    
    public static function getWebPageAddress() {
        if (self::isSecure()) {
            return 'https://' . $_SERVER['HTTP_HOST'] . '/' . self::getSubDomain();
        } else {
            return 'http://' . $_SERVER['HTTP_HOST'] . '/' . self::getSubDomain();
        }        
    }
    
    public static function getSubDomain() {
        return (new self)->subDomain;
    }
    
    public static function getView() {
        return (new self)->view;
    }
    
    public static function getAction() {
        return (new self)->action;
    }
    
}
