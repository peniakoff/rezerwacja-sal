<?php

use App\Models\Requests;

function route($route) {
    echo $route;
}

function asset($file) {
    echo Requests::getWebPageAddress() . '/' . $file;
}

function getSubDomain() {
    echo Requests::getSubDomain();
}

function redirect($url) {
    header("Location: {$url}");
    exit();
}