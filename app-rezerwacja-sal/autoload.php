<?php

require __DIR__ . '/config/database.php';
require __DIR__ . '/app/Controllers/app.php';
require __DIR__ . '/app/Controllers/helpers.php';

$db = \App\Models\Database::getInstance();
$session = new App\Models\Session;

require __DIR__ . '/routes/web.php';
