<?php

/*
 * @var array
 */
$viewRequest = [
    '' => ['default', 'home'],
    'show' => ['default', 'home'],
    'user' => ['default', 'user/default'],
    'login' => ['login'],
    'rooms' => ['default', 'rooms'],
    'errors' => ['errors', '404']
];

switch (App\Models\Requests::getView()) {
    case 'user':
        App\Models\Routes::user();
        break;
    case 'login':
        App\Models\Routes::login();
        break;
    default:
        break;
}

/*
 * @var array
 */
$actionRequest = [
    'login' => ['enter', 0],
    'user' => ['logout', 0],
    'user' => ['book', 2],
    'user' => ['check', 2],
    'user' => ['remove', 2],
    'show' => ['room', 2]
];

App\Models\Action::loginAction();

/*
 * New instance for view of the website
 * 
 * @param array with the names of requested view
 */
$view = new App\Models\Views($viewRequest);
