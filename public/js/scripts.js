var window,
    location,
    document,
    jQuery;

$(document).ready(function () {
    'use strict';
    $('.dropdown-button').dropdown();
    $('select').material_select();
});

(function ($) {
    'use strict';
    $(function () {
        $('.button-collapse').sideNav();
    }); // end of document ready
}(jQuery));

jQuery.datetimepicker.setLocale('pl');

jQuery('#dateTime').datetimepicker({
    format: 'Y-m-d',
    minDate: 0,
    lang: 'pl',
    step: 15,
    dayOfWeekStart: 1,
    timepicker: false
});

jQuery('#dateTimeStart').datetimepicker({
    format: 'Y-m-d H:i',
    minDate: 0,
    lang: 'pl',
    step: 15,
    dayOfWeekStart: 1,
    onShow: function (ct) {
        'use strict';
        this.setOptions({
            maxDate: jQuery('#dateTimeEnd').val() ? jQuery('#dateTimeEnd').val() : false
        });
    }
});

jQuery('#dateTimeEnd').datetimepicker({
    format: 'Y-m-d H:i',
    lang: 'pl',
    step: 15,
    dayOfWeekStart: 1,
    onShow: function (ct) {
        'use strict';
        this.setOptions({
            minDate: jQuery('#dateTimeStart').val() ? jQuery('#dateTimeStart').val() : false
        });
    }
});

function getRoomUrl(id, url) {
    'use strict';
    var value = document.getElementById(id).value;
    if (value === null || value === "") {
        return;
    }
    if (url !== '') {
        url = '/' + url;
    }
    window.location = url + '/show/room/' + value;
}

function getRoomCheck(id1, id2, id3, url) {
    'use strict';
    var roomNumber = document.getElementById(id1).value,
        startTime = document.getElementById(id2).value,
        endTime = document.getElementById(id3).value;
    if (roomNumber === null || roomNumber === "" || startTime === null || startTime === "" || endTime === null || endTime === "") {
        return;
    }
    if (url !== '') {
        url = '/' + url;
    }
    window.location = url + '/user/check/' + roomNumber + '/' + startTime + '/' + endTime;
}
